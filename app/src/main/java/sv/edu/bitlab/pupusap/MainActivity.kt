package sv.edu.bitlab.pupusap

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(){
    var contadorllama = hashMapOf(
        total to 0
    )

    var num1 :Button?=null
    var num2 :Button?=null
    var num3 :Button?=null
    var num4 :Button?=null
    var num5 :Button?=null
    var num6 :Button?=null
    var num7 :Button?=null
    var num8 :Button?=null
    var num9 :Button?=null
    var loadingContainer :View?= null

    var sendButton :Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        num1 = findViewById(R.id.num1)
        num2 = findViewById(R.id.num2)
        num3 = findViewById(R.id.num3)

        num1!!.setOnClickListener { addnum(1) }
        num2!!.setOnClickListener { addnum(2) }
        num3!!.setOnClickListener { addnum(3) }


        num4 = findViewById(R.id.num4)
        num5 = findViewById(R.id.num5)
        num6 = findViewById(R.id.num6)

        num4!!.setOnClickListener { addnum(4) }
        num5!!.setOnClickListener { addnum(5) }
        num6!!.setOnClickListener { addnum(6) }

        num7 = findViewById(R.id.num7)
        num8 = findViewById(R.id.num8)
        num9 = findViewById(R.id.num9)

        sendButton = findViewById(R.id.sendButton)
        sendButton!!.setOnClickListener { showLoading(true) }
        sendButton!!.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        loadingContainer = findViewById(R.id.loadingContainer)
        loadingContainer!!.setOnClickListener { showLoading(false) }
        setActionBar(null)
    }

    fun addnum(can: Int) {
        contadorllama[can] = contadorllama[can]!! + 1
    }

    fun showLoading(show: Boolean) {
        val visibility = if(show) View.VISIBLE else View.GONE
        loadingContainer!!.visibility = visibility
    }

    fun showcont(show: Boolean) {
        val visibility = if(show) View.VISIBLE else View.GONE
        loadingContainer!!.visibility = visibility

    companion object{
         val num1 = 1
         val num2 = 2
         val num3 = 3
         val num4 = 4
         val num5 = 5
         val num6 = 6
         val num7 = 7
         val num8 = 8
         val num9 = 9
        var total = 0
    }

}
